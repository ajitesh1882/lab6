# CSE 6230, Fall 2014: Lab 6: UVA/UVM and MPI+CUDA #
Team: Ajitesh Jain and Srinivas Eswar

For detailed instructions, see the wiki page for this assignment: https://bitbucket.org/gtcse6230fa14/lab6/wiki/Home

Question 1) Difference in data transfer speeds with/without copies

n = 16777216 (~ 64.0 MiB)
benchmarkReverseWithCopies: Timing...
55 trials took 2.02002 seconds.
Reversal with explicit copies: 0.0367276 seconds (3.65441 effective GB/s)

benchmarkReverseWithoutCopies: Timing...
122 trials took 2.01275 seconds.
Reversal without explicit copies: 0.0164979 seconds (8.13543 effective GB/s)

________________________________________________________________________________

Question 2) Difference in data transfer with/without using intermediate buffer

n = 16777216 (~ 64.0 MiB)
Test 1: Buffering GPU-to-GPU copy using CPU memory...
  [16 trials]
GPU-to-GPU copy, buffered through CPU memory: 0.0648677 seconds (2.0691 GB/s)

Test 2: Direct GPU-to-GPU copy...
  [26 trials]
Direct GPU-to-GPU copy: 0.039741 seconds (3.37731 GB/s)

________________________________________________________________________________

Question 3) Distributed Matrix Multiply


mm1d-blas
Using nodes:
jinx8
jinx9

========================================
Problem dimension: n = 4096
Number of MPI ranks: 2
Number of trials: 3
Time per trial (max over all processes): 0.487713 seconds
Computation time per trial (max over all processes): 0.442235 seconds
Effective performance: 281.8 GFLOP/s
========================================

mm1d-cuda with unpinned memory
Using nodes:
jinx8
jinx9

========================================
Problem dimension: n = 4096
Number of MPI ranks: 2
Number of trials: 5
Time per trial (max over all processes): 0.211025 seconds
Computation time per trial (max over all processes): 0.17098 seconds
Effective performance: 651.3 GFLOP/s
========================================
________________________________________________________________________________
Question 4) Implementing Distributed Matrix Multiply with pinned memory

mm1d-cuda with pinned memory and implicit transfers
Using nodes:
jinx8
jinx9

========================================
Problem dimension: n = 4096
Number of MPI ranks: 2
Number of trials: 3
Time per trial (max over all processes): 12.5161 seconds
Computation time per trial (max over all processes): 12.4714 seconds
Effective performance: 11.0 GFLOP/s
========================================
________________________________________________________________________________

mm1d-cuda with pinned memory and explicit transfers
Using nodes:
jinx8
jinx9

========================================
Problem dimension: n = 4096
Number of MPI ranks: 2
Number of trials: 6
Time per trial (max over all processes): 0.199747 seconds
Computation time per trial (max over all processes): 0.152647 seconds
Effective performance: 688.1 GFLOP/s
========================================
